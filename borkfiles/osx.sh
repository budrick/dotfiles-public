# ok brew

# ok brew the_silver_searcher
# ok brew htop
# ok brew git
# ok brew tmux
# ok brew reattach-to-user-namespace
# ok brew smartmontools

# Trackpad: enable tap to click for this user and for the login screen
ok defaults com.apple.driver.AppleBluetoothMultitouch.trackpad Clicking bool true
ok defaults -g com.apple.mouse.tapBehavior integer 1

# Save screenshots to the desktop
ok directory "${HOME}/Desktop/Screenshots"
ok defaults com.apple.screencapture location string "${HOME}/Desktop/Screenshots"


# Avoid creating .DS_Store files on network volumes
ok defaults com.apple.desktopservices DSDontWriteNetworkStores bool true

# Don’t automatically rearrange Spaces based on most recent use
ok defaults com.apple.dock mru-spaces bool false

# Don't automatically hide and show the Dock
ok defaults com.apple.dock autohide bool false

# Prevent Time Machine from prompting to use new hard drives as backup volume
ok defaults com.apple.TimeMachine DoNotOfferNewDisksForBackup bool true

# Finder: show all filename extensions
ok defaults -g AppleShowAllExtensions bool true

# Enable debug menu in App Store
ok defaults com.apple.appstore ShowDebugMenu bool true

# Don't open Photos when inserting devices
ok defaults com.apple.ImageCapture disableHotPlug bool true

#Add a context menu item for showing the Web Inspector in web views
ok defaults NSGlobalDomain WebKitDeveloperExtras bool true

#Show the ~/Library folder
bake chflags nohidden ~/Library

# Expand the save and print dialogs by default
ok defaults -g NSNavPanelExpandedStateForSaveMode bool true
ok defaults -g PMPrintingExpandedStateForPrint bool true
ok defaults -g PMPrintingExpandedStateForPrint2 bool true

# Show control chars in NSText controls
ok defaults -g NSTextShowsControlCharacters bool true

# Dark mode - TODO: wait for bork to update so that sudo can happen automatically for this one
# ok defaults /Library/Preferences/.GlobalPreferences AppleInterfaceTheme Dark


