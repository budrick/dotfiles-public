# Install vundle and install all bundles
ok directory ~/.vim/bundle/
git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
ok directory ~/.vim/bundle/Vundle.vim
vim +PluginInstall +qall
