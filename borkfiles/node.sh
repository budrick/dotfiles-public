ok directory ~/.node
if [ ! -d ~/.node/lib/node_modules/npm ]; then
    echo prefix=~/.node >> ~/.npmrc
    curl -L https://www.npmjs.com/install.sh | sh
fi

if [ -d ~/.node/lib/node_modules/npm/ ]; then
    ok npm gulp
    ok npm webpack
    ok npm npm-check-updates
fi

