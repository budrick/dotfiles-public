# DFDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# Set up basic symlinks
for file in $DFDIR/dotfiles/*; do
	ok symlink ~/.$(basename $file) $file
done

for file in $DFDIR/dotconfig/*; do
    ok directory ~/.config
    ok symlink ~/.config/$(basename $file) $file
done
