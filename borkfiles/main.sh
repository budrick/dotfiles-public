# Where we look for source files
DFDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
include dotfiles.sh
include vim.sh
# node setup isn't universally required; leave only as example
# include node.sh

case $OSTYPE in
  darwin*)
    include osx.sh
    ;;
esac
