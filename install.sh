#!/usr/bin/env bash
getDir () {
  fname=$1
  while [ -h "$fname" ]; do
    dir=$(cd -P "$(dirname "$fname")" && pwd)
    fname=$(readlink $fname)
    [[ $fname != /* ]] && fname="$dir/$fname"
  done
  echo "$(cd "$(dirname $fname)" && pwd -P)"
}
# used by loader to find core/ and stdlib/
BORK_SOURCE_DIR="$(cd $(getDir ${BASH_SOURCE[0]})/.. && pwd -P)"
BORK_SCRIPT_DIR=$PWD
BORK_WORKING_DIR=$PWD
operation="satisfy"
case "$1" in
  status) operation="$1"
esac
is_compiled () { return 0; }
arguments () {
  op=$1
  shift
  case $op in
    get)
      key=$1
      shift
      value=
      while [ -n "$1" ] && [ -z "$value" ]; do
        this=$1
        shift
        if [ ${this:0:2} = '--' ]; then
          tmp=${this:2}       # strip off leading --
          echo "$tmp" | grep -E '=' > /dev/null
          if [ "$?" -eq 0 ]; then
            param=${tmp%%=*}    # everything before =
            val=${tmp##*=}      # everything after =
          else
            param=$tmp
            val="true"
          fi
        if [ "$param" = $key ]; then value=$val; fi
        fi
      done
      [ -n $value ] && echo "$value"
      ;;
    *) return 1 ;;
  esac
}
bag () {
  action=$1
  varname=$2
  shift 2
  if [ "$action" != "init" ]; then
    length=$(eval "echo \${#$varname[*]}")
    last=$(( length - 1 ))
  fi
  case "$action" in
    init) eval "$varname=( )" ;;
    push) eval "$varname[$length]=\"$1\"" ;;
    pop) eval "unset $varname[$last]" ;;
    read)
      [ "$length" -gt 0 ] && echo $(eval "echo \${$varname[$last]}") ;;
    size) echo $length ;;
    filter)
      index=0
      (( limit=$2 ))
      [ "$limit" -eq 0 ] && limit=-1
      while [ "$index" -lt $length ]; do
        line=$(eval "echo \${$varname[$index]}")
        if str_matches "$line" "$1"; then
          [ -n "$3" ] && echo $index || echo $line
          [ "$limit" -ge $index ] && return
        fi
        (( index++ ))
      done ;;
    find) echo $(bag filter $varname $1 1) ;;
    index) echo $(bag filter $varname $1 1 1) ;;
    set)
      idx=$(bag index $varname "^$1=")
      [ -z "$idx" ] && idx=$length
      eval "$varname[$idx]=\"$1=$2\""
      ;;
    get)
      line=$(bag filter $varname "^$1=" 1)
      echo "${line##*=}" ;;
    print)
      index=0
      while [ "$index" -lt $length ]; do
        eval "echo \"\${$varname[$index]}\""
        (( index++ ))
      done
      ;;
    *) return 1 ;;
  esac
}
bake () { eval "$*"; }
type_help () {
  if [ -s $1 ]; then
    desc=$(. $1 desc)
    i=0
    summary=
    usage=
    while read -r line; do
      [ "$i" -eq 0 ] && summary=$line || usage=$([ -n "$usage" ] && echo "$usage"; echo "$line")
      (( i ++ ))
    done <<< "$desc"
    echo "$(printf '%15s' $(basename $1 '.sh')): $summary"
    if [ -n "$usage" ]; then
      while read -r line; do
        echo "                 $line"
      done <<< "$usage"
    fi
  else
    echo "undefined type: $(basename $1 '.sh')"
  fi
}
has_curl () {
    needs_exec "curl"
}
http_head_cmd () {
    url=$1
    shift 1
    has_curl
    if [ "$?" -eq 0 ]; then
        echo "curl -sIL \"$url\""
    else
        echo "curl not found; wget support not implemented yet"
        return 1
    fi
}
http_header () {
    header=$1
    headers=$2
    echo "$headers" | grep "$header" | tr -s ' ' | cut -d' ' -f2 | tail -1
}
http_get_cmd () {
    url=$1
    target=$2
    has_curl
    if [ "$?" -eq 0 ]; then
        echo "curl -sLo \"$target\" \"$url\" &> /dev/null"
    else
        echo "curl not found; wget support not implemented yet"
        return 1
    fi
}
md5cmd () {
  case $1 in
    Darwin|FreeBSD)
      [ -z "$2" ] && echo "md5" || echo "md5 -q $2"
      ;;
    Linux)
      [ -z "$2" ] && arg="" || arg="$2 "
      echo "md5sum $arg| awk '{print \$1}'"
      ;;
    *) return 1 ;;
  esac
}
satisfying () { [ "$operation" == "satisfy" ]; }
permission_cmd () {
  case $1 in
    Linux) echo "stat --printf '%a'" ;;
    Darwin|FreeBSD) echo "stat -f '%Lp'" ;;
    *) return 1 ;;
  esac
}
permission_cmd_dir () {
  case $1 in
    Linux) echo "stat --printf '%U\\n%G\\n%a'" ;;
    Darwin|FreeBSD) echo "stat -f '%Su%n%Sg%n%Lp'" ;;
    *) return 1 ;;
  esac
}
STATUS_OK=0
STATUS_FAILED=1
STATUS_MISSING=10
STATUS_OUTDATED=11
STATUS_PARTIAL=12
STATUS_MISMATCH_UPGRADE=13
STATUS_MISMATCH_CLOBBER=14
STATUS_CONFLICT_UPGRADE=20
STATUS_CONFLICT_CLOBBER=21
STATUS_CONFLICT_HALT=25
STATUS_BAD_ARGUMENTS=30
STATUS_FAILED_ARGUMENTS=31
STATUS_FAILED_ARGUMENT_PRECONDITION=32
STATUS_FAILED_PRECONDITION=33
STATUS_UNSUPPORTED_PLATFORM=34
if [ ! -z "$BORK_COLOR" ]; then
    GREEN="\033[92m"
    RED="\033[91m"
    YELLOW="\033[93m"
    RESET="\033[39m"
fi
_present_status_for () {
  case "$1" in
    $STATUS_OK)                           echo -e "${GREEN}ok${RESET}" ;;
    $STATUS_FAILED)                       echo -e "${RED}failed${RESET}" ;;
    $STATUS_MISSING)                      echo -e "${YELLOW}missing${RESET}" ;;
    $STATUS_OUTDATED)                     echo -e "${YELLOW}outdated${RESET}" ;;
    $STATUS_PARTIAL)                      echo -e "${YELLOW}partial${RESET}" ;;
    $STATUS_MISMATCH_UPGRADE)             echo -e "${YELLOW}mismatch (upgradable)${RESET}" ;;
    $STATUS_MISMATCH_CLOBBER)             echo -e "${RED}mismatch (clobber required)${RESET}" ;;
    $STATUS_CONFLICT_UPGRADE)             echo -e "${YELLOW}conflict (upgradable)${RESET}" ;;
    $STATUS_CONFLICT_CLOBBER)             echo -e "${RED}conflict (clobber required)${RESET}" ;;
    $STATUS_CONFLICT_HALT)                echo -e "${RED}conflict (unresolvable)${RESET}" ;;
    $STATUS_BAD_ARGUMENT)                 echo -e "${RED}error (bad arguments)${RESET}" ;;
    $STATUS_FAILED_ARGUMENTS)             echo -e "${RED}error (failed arguments)${RESET}" ;;
    $STATUS_FAILED_ARGUMENT_PRECONDITION) echo -e "${RED}error (failed argument precondition)${RESET}" ;;
    $STATUS_FAILED_PRECONDITION)          echo -e "${RED}error (failed precondition)${RESET}" ;;
    $STATUS_UNSUPPORTED_PLATFORM)         echo -e "${RED}error (unsupported platform)${RESET}" ;;
    *)                                    echo -e "${RED}unknown status${RESET}: $1" ;;
  esac
}
_absent_status_for () {
  case "$1" in
    $STATUS_OK)      echo -e "${YELLOW}present${RESET}" ;;
    $STATUS_MISSING) echo -e "${GREEN}no${RESET}" ;;
    *)               _present_status_for $1 ;;
  esac
}
_status_for () {
  if [ "$1" = "ok" ]; then
    _present_status_for $2
  else
    _absent_status_for $2
  fi
}
needs_exec () {
  [ -z "$1" ] && return 1
  [ -z "$2" ] && running_status=0 || running_status=$2
  path=$(bake "which $1")
  if [ "$?" -gt 0 ]; then
    echo "missing required exec: $1"
    retval=$((running_status+1))
    return $retval
  else return $running_status
  fi
}
platform=$(uname -s)
is_platform () {
  [ "$platform" = $1 ]
  return $?
}
platform_is () {
  [ "$platform" = $1 ]
  return $?
}
baking_platform=
baking_platform_is () {
  [ -z "$baking_platform" ] && baking_platform=$(bake uname -s)
  [ "$baking_platform" = $1 ]
  return $?
}
get_baking_platform () {
  bake uname -s
}
get_baking_platform_release() {
  bake uname -r
}
str_contains () {
  str_matches "$1" "^$2\$"
}
str_get_field () {
  echo $(echo "$1" | awk '{print $'"$2"'}')
}
str_item_count () {
  accum=0
  for item in $1; do
    ((accum++))
  done
  echo $accum
}
str_matches () {
  $(echo "$1" | grep -E "$2" > /dev/null)
  return $?
}
str_replace () {
  echo $(echo "$1" | sed -E 's|'"$2"'|'"$3"'|g')
}
strip_blanks () {
  awk '!/^($|[[:space:]]*#)/{print $0}' <&0
}
bork_performed_install=0
bork_performed_upgrade=0
bork_performed_remove=0
bork_performed_error=0
bork_any_updated=0
did_install () { [ "$bork_performed_install" -eq 1 ] && return 0 || return 1; }
did_upgrade () { [ "$bork_performed_upgrade" -eq 1 ] && return 0 || return 1; }
did_remove () { [ "$bork_performed_remove" -eq 1 ] && return 0 || return 1; }
did_update () {
  if did_install; then return 0
  elif did_upgrade; then return 0
  elif did_remove; then return 0
  else return 1
  fi
}
did_error () { [ "$bork_performed_error" -gt 0 ] && return 0 || return 1; }
any_updated () { [ "$bork_any_updated" -gt 0 ] && return 0 || return 1; }
_changes_reset () {
  bork_performed_install=0
  bork_performed_upgrade=0
  bork_performed_remove=0
  bork_performed_error=0
  last_change_type=
}
_changes_complete () {
  status=$1
  action=$2
  if [ "$status" -gt 0 ]; then bork_performed_error=1
  elif [ "$action" = "install" ]; then bork_performed_install=1
  elif [ "$action" = "upgrade" ]; then bork_performed_upgrade=1
  elif [ "$action" = "remove" ]; then bork_performed_remove=1
  fi
  if did_update; then bork_any_updated=1 ;fi
  [ "$status" -gt 0 ] && echo "* failure"
}
_changes_expected () {
  action=$1
  assertion=$2
  shift 2
  argstr=$*
  _callback_run "change" "$assertion" "$argstr"
  _callback_run "$action" "$assertion" "$argstr"
  unset bork_will_change
  unset bork_will_install
  unset bork_will_upgrade
  unset bork_will_remove
}
_callback_defined () { declare -F "$1" > /dev/null; }
_callback_run () {
  callback=$1
  assertion=$2
  shift 2
  argstr=$*
  _callback_defined "bork_will_$callback" && eval "bork_will_$callback $assertion $argstr"
  _callback_defined "bork_will_${callback}_any" && eval "bork_will_${callback}_any $assertion $argstr"
}
destination () {
  echo "deprecation warning: 'destination' utility will be removed in a future version - use 'cd' instead" 1>&2
  cd $1
}
bag init include_directories
bag push include_directories "$BORK_SCRIPT_DIR"
include () {
    incl_script="$(bag read include_directories)/$1"
    if [ -e $incl_script ]; then
        target_dir=$(dirname $incl_script)
        bag push include_directories "$target_dir"
        case $operation in
            compile) compile_file "$incl_script" ;;
            *) . $incl_script ;;
        esac
        bag pop include_directories
    else
        echo "include: $incl_script: No such file" 1>&2
        exit 1
    fi
    return 0
}
_source_runner () {
  if is_compiled; then echo "$1"
  else echo ". $1"
  fi
}
_bork_check_failed=0
check_failed () { [ "$_bork_check_failed" -gt 0 ] && return 0 || return 1; }
_checked_len=0
_checking () {
  type=$1
  shift
  check_str="$type: $*"
  _checked_len=${#check_str}
  echo -n "$check_str"$'\r'
}
_checked () {
  report="$*"
  (( pad=$_checked_len - ${#report} + 10 ))
  i=1
  while [ "$i" -le $pad ]; do
    report+=" "
    (( i++ ))
  done
  echo "$report"
}
_conflict_approve () {
  if [ -n "$BORK_CONFLICT_RESOLVE" ]; then
    return $BORK_CONFLICT_RESOLVE
  fi
  echo
  echo "== Warning! Assertion: $*"
  echo "Attempting to satisfy has resulted in a conflict.  Satisfying this may overwrite data."
  _yesno "Do you want to continue?"
  return $?
}
_yesno () {
  answered=0
  answer=
  while [ "$answered" -eq 0 ]; do
    read -p "$* (yes/no) " answer
    if [[ "$answer" == 'y' || "$answer" == "yes" || "$answer" == "n" || "$answer" == "no" ]]; then
      answered=1
    else
      echo "Valid answers are: yes y no n" >&2
    fi
  done
  [[ "$answer" == 'y' || "$answer" == 'yes' ]]
}
_make_change () {
  change_type=$1
  _changes_expected "$change_type" "$assertion" "$argstr"
  eval "$(_source_runner $fn) $change_type $quoted_argstr"
  _changes_complete $? $change_type
  last_change_type=$change_type
}
assert () {
  assert_mode=$1
  shift
  assertion=$1
  shift
  _bork_check_failed=0
  _changes_reset
  fn=$(_lookup_type $assertion)
  if [ -z "$fn" ]; then
    echo "not found: $assertion" 1>&2
    return 1
  fi
  argstr=$*
  quoted_argstr=
  while [ -n "$1" ]; do
    quoted_argstr=$(echo "$quoted_argstr '$1'")
    shift
  done
  case $operation in
    echo) echo "$fn $argstr" ;;
    status)
      _checking "checking" $assertion $argstr
      output=$(eval "$(_source_runner $fn) status $quoted_argstr")
      status=$?
      _checked "$(_status_for $assert_mode $status): $assertion $argstr"
      [ "$status" -eq 1 ] && _bork_check_failed=1
      [ "$status" -ne 0 ] && [ -n "$output" ] && echo "$output"
      [ "$assert_mode" = 'no' ] && [ $status -eq $STATUS_MISSING ] && return 0
      [ "$status" -ne 0 ] && BORK_EXIT_STATUS=$status
      return $status
      ;;
    satisfy)
      _checking "checking" $assertion $argstr
      status_output=$(eval "$(_source_runner $fn) status $quoted_argstr")
      status=$?
      _checked "$(_status_for $assert_mode $status): $assertion $argstr"
      case $status in
        0)
          [ $assert_mode = 'no' ] && _make_change 'remove'
          ;;
        1)
          _bork_check_failed=1
          echo "$status_output"
          ;;
        10)
          [ $assert_mode = 'ok' ] && _make_change 'install'
          ;;
        11|12|13)
          if [ $assert_mode = 'ok' ]; then
            echo "$status_output"
            _make_change 'upgrade'
          elif [ $assert_mode = 'no' ]; then
            _make_change 'remove'
          fi
          ;;
        20)
          echo "$status_output"
          _conflict_approve $assertion $argstr
          if [ "$?" -eq 0 ]; then
            echo "Resolving conflict..."
            [ $assert_mode = 'ok' ] && _make_change 'upgrade'
            [ $assert_mode = 'no' ] && _make_change 'remove'
          else
            echo "Conflict unresolved."
          fi
          ;;
        *)
          echo "-- sorry, bork doesn't handle this response yet"
          echo "$status_output"
          ;;
      esac
      if did_update; then
        echo "verifying $last_change_type: $assertion $argstr"
        output=$(eval "$(_source_runner $fn) status $quoted_argstr")
        status=$?
        if [ "$status" -gt 0 ] && [ "$assert_mode" = 'ok' ]; then
          echo "* $last_change_type failed"
          _checked "$(_status_for $assert_mode $status)"
          echo "$output"
        elif [ "$status" -ne "$STATUS_MISSING" ] && [ "$assert_mode" = 'no' ]; then
          echo "* $last_change_type failed"
          _checked "$(_status_for $assert_mode $status)"
          echo "$output"
        else
          echo "* success"
        fi
        return 1
      fi
      ;;
  esac
}
ok () {
  assert 'ok' $*
}
no () {
  assert 'no' $*
}
bag init bork_assertion_types
register () {
  file=$1
  type=$(basename $file '.sh')
  if [ -e "$BORK_SCRIPT_DIR/$file" ]; then
    file="$BORK_SCRIPT_DIR/$file"
  else
    exit 1
  fi
  bag set bork_assertion_types $type $file
}
_lookup_type () {
  assertion=$1
  if is_compiled; then
    echo "type_$assertion"
    return
  fi
  fn=$(bag get bork_assertion_types $assertion)
  if [ -n "$fn" ]; then
    echo "$fn"
    return
  fi
  bork_official="$BORK_SOURCE_DIR/types/$(echo $assertion).sh"
  if [ -e "$bork_official" ]; then
    echo "$bork_official"
    return
  fi
  local_script="$BORK_SCRIPT_DIR/$assertion"
  if [ -e "$local_script" ]; then
    echo "$local_script"
    return
  fi
  return 1
}
# Where we look for source files
DFDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
# DFDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# Set up basic symlinks
for file in $DFDIR/dotfiles/*; do
# /Users/matt/dotfiles-public/bork/types/symlink.sh
type_symlink () {
  action=$1
  target=$2
  source=$3
  case "$action" in
    desc)
      echo "assert presence and target of a symlink"
      echo "> symlink .vimrc ~/code/dotfiles/configs/vimrc"
      ;;
    status)
      bake [ ! -e "$target" ] && return $STATUS_MISSING
      if bake [ ! -h "$target" ]; then
        echo "not a symlink: $target"
        return $STATUS_CONFLICT_CLOBBER
      else
        existing_source=$(bake readlink \"$target\")
        if [ "$existing_source" != "$source" ]; then
          echo "received source for existing symlink: $existing_source"
          echo "expected source for symlink: $source"
          return $STATUS_MISMATCH_UPGRADE
        fi
      fi
      return $STATUS_OK
      ;;
    install|upgrade)
      bake ln -sf "$source" "$target" ;;
    *) return 1;;
  esac
}
ok symlink ~/.$(basename $file) $file
done

for file in $DFDIR/dotconfig/*; do
# /Users/matt/dotfiles-public/bork/types/directory.sh
type_directory () {
  action=$1
  dir=$2
  shift 2
  owner=$(arguments get owner $*)
  group=$(arguments get group $*)
  mode=$(arguments get mode $*)
  target_platform=$(get_baking_platform)
  case "$action" in
    desc)
      printf '%s\n' \
        'asserts presence of a directory' \
        '* directory path [options]' \
        '--owner=user-name' \
        '--group=group-name' \
        '--mode=mode' \
        '> directory ~/.ssh --mode=700'
      ;;
    status)
      bake [ -e "${dir}" ] || return $STATUS_MISSING
      bake [ -d "${dir}" ] || {
        echo "target exists as non-directory"
        return $STATUS_CONFLICT_CLOBBER
      }
      mismatch=false
      if [[ -n ${owner} || -n ${group} || -n ${mode} ]]; then
        dir_stat=($(bake $(permission_cmd_dir $target_platform) "${dir}"))
        if [[ -n ${owner} && ${dir_stat[0]} != ${owner} ]]; then
          printf '%s owner: %s\n' \
            'expected' "${owner}" \
            'received' "${dir_stat[0]}"
          mismatch=true
        fi
        if [[ -n ${group} && ${dir_stat[1]} != ${group} ]]; then
          printf '%s group: %s\n' \
            'expected' "${group}" \
            'received' "${dir_stat[1]}"
          mismatch=true
        fi
        if [[ -n ${mode} && ${dir_stat[2]} != ${mode} ]]; then
          printf '%s mode: %s\n' \
            'expected' "${mode}" \
            'received' "${dir_stat[2]}"
          mismatch=true
        fi
      fi
      if ${mismatch}; then
        return "${STATUS_MISMATCH_UPGRADE}"
      fi
      return "${STATUS_OK}"
      ;;
    install|upgrade)
      if baking_platform_is "Darwin"; then
        inst_cmd=( install -d )
      else
        inst_cmd=( install -C -d )
      fi
      [[ -z ${owner} && -z ${group} ]] || inst_cmd=( sudo "${inst_cmd[@]}" )
      [[ -z ${owner} ]] || inst_cmd+=( -o "${owner}" )
      [[ -z ${group} ]] || inst_cmd+=( -g "${group}" )
      [[ -z ${mode} ]] || inst_cmd+=( -m "${mode}" )
      bake "${inst_cmd[@]}" "${dir}"
      ;;
    remove)
      bake "rm -r ${dir}"
      ;;
    *) return 1 ;;
  esac
}
ok directory ~/.config
ok symlink ~/.config/$(basename $file) $file
done
# Install vundle and install all bundles
# /Users/matt/dotfiles-public/bork/types/directory.sh
type_directory () {
  action=$1
  dir=$2
  shift 2
  owner=$(arguments get owner $*)
  group=$(arguments get group $*)
  mode=$(arguments get mode $*)
  target_platform=$(get_baking_platform)
  case "$action" in
    desc)
      printf '%s\n' \
        'asserts presence of a directory' \
        '* directory path [options]' \
        '--owner=user-name' \
        '--group=group-name' \
        '--mode=mode' \
        '> directory ~/.ssh --mode=700'
      ;;
    status)
      bake [ -e "${dir}" ] || return $STATUS_MISSING
      bake [ -d "${dir}" ] || {
        echo "target exists as non-directory"
        return $STATUS_CONFLICT_CLOBBER
      }
      mismatch=false
      if [[ -n ${owner} || -n ${group} || -n ${mode} ]]; then
        dir_stat=($(bake $(permission_cmd_dir $target_platform) "${dir}"))
        if [[ -n ${owner} && ${dir_stat[0]} != ${owner} ]]; then
          printf '%s owner: %s\n' \
            'expected' "${owner}" \
            'received' "${dir_stat[0]}"
          mismatch=true
        fi
        if [[ -n ${group} && ${dir_stat[1]} != ${group} ]]; then
          printf '%s group: %s\n' \
            'expected' "${group}" \
            'received' "${dir_stat[1]}"
          mismatch=true
        fi
        if [[ -n ${mode} && ${dir_stat[2]} != ${mode} ]]; then
          printf '%s mode: %s\n' \
            'expected' "${mode}" \
            'received' "${dir_stat[2]}"
          mismatch=true
        fi
      fi
      if ${mismatch}; then
        return "${STATUS_MISMATCH_UPGRADE}"
      fi
      return "${STATUS_OK}"
      ;;
    install|upgrade)
      if baking_platform_is "Darwin"; then
        inst_cmd=( install -d )
      else
        inst_cmd=( install -C -d )
      fi
      [[ -z ${owner} && -z ${group} ]] || inst_cmd=( sudo "${inst_cmd[@]}" )
      [[ -z ${owner} ]] || inst_cmd+=( -o "${owner}" )
      [[ -z ${group} ]] || inst_cmd+=( -g "${group}" )
      [[ -z ${mode} ]] || inst_cmd+=( -m "${mode}" )
      bake "${inst_cmd[@]}" "${dir}"
      ;;
    remove)
      bake "rm -r ${dir}"
      ;;
    *) return 1 ;;
  esac
}
ok directory ~/.vim/bundle/
git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
ok directory ~/.vim/bundle/Vundle.vim
vim +PluginInstall +qall
# node setup isn't universally required; leave only as example
# include node.sh

case $OSTYPE in
darwin*)
# ok brew

# ok brew the_silver_searcher
# ok brew htop
# ok brew git
# ok brew tmux
# ok brew reattach-to-user-namespace
# ok brew smartmontools

# Trackpad: enable tap to click for this user and for the login screen
# /Users/matt/dotfiles-public/bork/types/defaults.sh
type_defaults () {
  action=$1
  domain=$2
  key=$3
  desired_type=$4
  [ "$desired_type" = "int" ] && desired_type="integer"
  shift 4
  if [ "${desired_type:0:4}" = "dict" ]; then
    desired_val=$*
  else
    desired_val=$1
  fi
  case $action in
    desc)
      echo "asserts settings for macOS's 'defaults' system"
      echo "* defaults domain key type value"
      echo "> defaults com.apple.dock autohide bool true"
      ;;
    status)
      needs_exec "defaults" || return $STATUS_FAILED_PRECONDITION
      current_val=$(bake defaults read $domain $key)
      [ "$?" -eq 1 ] && return $STATUS_MISSING
      current_type=$(str_get_field "$(bake defaults read-type $domain $key)" 3)
      conflict=
      if [ "$current_type" = "boolean" ]; then
        current_type="bool"
        case "$current_val" in
          0) current_val="false" ;;
          1|YES) current_val="true" ;;
        esac
      fi
      if [ "$current_type" = "dictionary" ]; then
        current_type="dict"
        bag init temp_defaults_value
        bag push temp_defaults_value "{"
        while [ -n "$1" ]; do
          key=$1
          shift
          next="$1"
          [ ${next:0:1} = '-' ] && shift
          value=$1
          shift
          bash push temp_defaults_value "  $key = $value"
        done
        bag push temp_defaults_value "}"
        desired_val=$(bag print temp_defaults_value)
      fi
      if [ "$desired_type" != $current_type ]; then
        conflict=1
        echo "expected type: $desired_type"
        echo "received type: $current_type"
      fi
      if [ "$current_val" != $desired_val ]; then
        conflict=1
        echo "expected value: $desired_val"
        echo "received value: $current_val"
      fi
      [ -n "$conflict" ] && return $STATUS_MISMATCH_UPGRADE
      return $STATUS_OK
      ;;
    install|upgrade)
      bake defaults write $domain $key "-$desired_type" $desired_val
      ;;
    remove)
      bake defaults delete $domain $key
      ;;
    *) return 1 ;;
  esac
}
ok defaults com.apple.driver.AppleBluetoothMultitouch.trackpad Clicking bool true
ok defaults -g com.apple.mouse.tapBehavior integer 1

# Save screenshots to the desktop
# /Users/matt/dotfiles-public/bork/types/directory.sh
type_directory () {
  action=$1
  dir=$2
  shift 2
  owner=$(arguments get owner $*)
  group=$(arguments get group $*)
  mode=$(arguments get mode $*)
  target_platform=$(get_baking_platform)
  case "$action" in
    desc)
      printf '%s\n' \
        'asserts presence of a directory' \
        '* directory path [options]' \
        '--owner=user-name' \
        '--group=group-name' \
        '--mode=mode' \
        '> directory ~/.ssh --mode=700'
      ;;
    status)
      bake [ -e "${dir}" ] || return $STATUS_MISSING
      bake [ -d "${dir}" ] || {
        echo "target exists as non-directory"
        return $STATUS_CONFLICT_CLOBBER
      }
      mismatch=false
      if [[ -n ${owner} || -n ${group} || -n ${mode} ]]; then
        dir_stat=($(bake $(permission_cmd_dir $target_platform) "${dir}"))
        if [[ -n ${owner} && ${dir_stat[0]} != ${owner} ]]; then
          printf '%s owner: %s\n' \
            'expected' "${owner}" \
            'received' "${dir_stat[0]}"
          mismatch=true
        fi
        if [[ -n ${group} && ${dir_stat[1]} != ${group} ]]; then
          printf '%s group: %s\n' \
            'expected' "${group}" \
            'received' "${dir_stat[1]}"
          mismatch=true
        fi
        if [[ -n ${mode} && ${dir_stat[2]} != ${mode} ]]; then
          printf '%s mode: %s\n' \
            'expected' "${mode}" \
            'received' "${dir_stat[2]}"
          mismatch=true
        fi
      fi
      if ${mismatch}; then
        return "${STATUS_MISMATCH_UPGRADE}"
      fi
      return "${STATUS_OK}"
      ;;
    install|upgrade)
      if baking_platform_is "Darwin"; then
        inst_cmd=( install -d )
      else
        inst_cmd=( install -C -d )
      fi
      [[ -z ${owner} && -z ${group} ]] || inst_cmd=( sudo "${inst_cmd[@]}" )
      [[ -z ${owner} ]] || inst_cmd+=( -o "${owner}" )
      [[ -z ${group} ]] || inst_cmd+=( -g "${group}" )
      [[ -z ${mode} ]] || inst_cmd+=( -m "${mode}" )
      bake "${inst_cmd[@]}" "${dir}"
      ;;
    remove)
      bake "rm -r ${dir}"
      ;;
    *) return 1 ;;
  esac
}
ok directory "${HOME}/Desktop/Screenshots"
ok defaults com.apple.screencapture location string "${HOME}/Desktop/Screenshots"


# Avoid creating .DS_Store files on network volumes
ok defaults com.apple.desktopservices DSDontWriteNetworkStores bool true

# Don’t automatically rearrange Spaces based on most recent use
ok defaults com.apple.dock mru-spaces bool false

# Don't automatically hide and show the Dock
ok defaults com.apple.dock autohide bool false

# Prevent Time Machine from prompting to use new hard drives as backup volume
ok defaults com.apple.TimeMachine DoNotOfferNewDisksForBackup bool true

# Finder: show all filename extensions
ok defaults -g AppleShowAllExtensions bool true

# Enable debug menu in App Store
ok defaults com.apple.appstore ShowDebugMenu bool true

# Don't open Photos when inserting devices
ok defaults com.apple.ImageCapture disableHotPlug bool true

#Add a context menu item for showing the Web Inspector in web views
ok defaults NSGlobalDomain WebKitDeveloperExtras bool true

#Show the ~/Library folder
bake chflags nohidden ~/Library

# Expand the save and print dialogs by default
ok defaults -g NSNavPanelExpandedStateForSaveMode bool true
ok defaults -g PMPrintingExpandedStateForPrint bool true
ok defaults -g PMPrintingExpandedStateForPrint2 bool true

# Show control chars in NSText controls
ok defaults -g NSTextShowsControlCharacters bool true

# Dark mode - TODO: wait for bork to update so that sudo can happen automatically for this one
# ok defaults /Library/Preferences/.GlobalPreferences AppleInterfaceTheme Dark


;;
esac
