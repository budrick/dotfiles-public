#!/usr/bin/env bash
DFDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

if [ ! -f bork/bin/bork ]; then
    git clone https://github.com/borksh/bork
fi

git -C bork pull

bork/bin/bork compile borkfiles/main.sh > install.sh
