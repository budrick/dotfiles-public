dotfiles
========

Bits and pieces, with a dash of [Bork](https://github.com/mattly/bork) to ease the pain of a new setup

### Installation

I don't recommend using this repository blindly, but I'm happy for it to serve as a useful example.

1. Fork this repository
2. Clone the repository
3. `cd` to the clone directory
4. `./install.sh`

### Building your own

If you want to change anything, make your edits in the `borkfiles` directory and then run `./build.sh`. This will fetch Bork if needed, and rebuild `install.sh`

