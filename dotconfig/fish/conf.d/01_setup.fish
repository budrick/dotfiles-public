test -x /opt/homebrew/bin/brew && eval "$(/opt/homebrew/bin/brew shellenv)"
command -q fnm && fnm env --use-on-cd --shell fish | source
test -d $HOME/.cargo/bin && fish_add_path -g $HOME/.cargo/bin
test -d $HOME/.local/bin && fish_add_path -g $HOME/.local/bin
