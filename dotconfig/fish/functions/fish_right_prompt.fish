function fish_right_prompt --description 'Write out the right prompt'
    set -l last_status $status
    set -l normal (set_color normal)
    set -l vcs_color (set_color brpurple)
    set -l prompt_status ""



    # Color the prompt in red on error
    if test $last_status -ne 0
        set status_color (set_color $fish_color_error)
        set prompt_status $status_color "[" $last_status "] " $normal
    end

    echo -s $vcs_color (fish_vcs_prompt) $normal ' ' $prompt_status $normal
end
